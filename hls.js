const childProcess = require('child_process')

function preprocess(filepath, output, subtitle = '', subtitleDelay) {
  console.log(filepath, output, subtitle, subtitleDelay)

  if (!subtitle.length) {
    subtitleDelay = ''
  }
  
  const spawn = childProcess.spawn
  const ls = spawn(`bash preprocess.sh ${filepath} ${output} ${subtitle} ${subtitleDelay}`, {
    shell: true,
  })

  return new Promise((resolve, reject) => {
    ls.stdout.on('data', (data) => {
      console.log(data.toString())
    })
    ls.stderr.on('data', (data) => {
      console.log(data.toString())
    })
    ls.on('exit', (code) => {
      if (code === 0) {
        resolve()
      } else {
        reject(new Error('Error when preprocessing!'))
      }
    })
  })
}

function generateHLS(filepath, output) {
  console.log(filepath, output)
  const spawn = childProcess.spawn
  const ls = spawn(`bash create-vod-hls.sh ${filepath} ${output}`, {
    shell: true,
  })

  return new Promise((resolve, reject) => {
    ls.stdout.on('data', (data) => {
      console.log(data.toString())
    })
    ls.stderr.on('data', (data) => {
      console.log(data.toString())
    })
    ls.on('exit', (code) => {
      if (code === 0) {
        resolve()
      } else {
        reject(new Error('Error when generating HLS stream!'))
      }
    })
  })
}

module.exports = {
  preprocess,
  generateHLS
}