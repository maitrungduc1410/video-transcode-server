FROM node:13-alpine

LABEL maintainer="Mai Trung Duc (maitrungduc1410@gmail.com)"

# Set environment variable for later use in the app
ENV IS_DOCKER 1

WORKDIR /app

COPY . .

# ffmpeg to get audio duration, bash to execute shell script
RUN apk add --no-cache ffmpeg bash tzdata

RUN npm install && npm install -g pm2

# log control
RUN pm2 install pm2-logrotate

CMD ["pm2-runtime", "ecosystem.config.js", "--env", "production"]