#!/usr/bin/env bash
set -e

source="${1}"
target="${2}"
subtitle="${3}"
subtitleDelay="${4}"
watermark=${HLS_WATERMARK_SOURCE}

# check if should sync subtitle
if (( $(echo "$subtitleDelay != 0" | bc -l) )); then
  echo "Start syncing subtitle"
  syncedSubtitle="${subtitle##*/}" # leave only last component of path
  syncedSubtitle="${subtitle%.*}"  # strip extension
  subExtension="${subtitle##*.}"
  syncedSubtitle="${syncedSubtitle}_synced.${subExtension}"

  ffmpeg -hide_banner -y -itsoffset ${subtitleDelay} -i ${subtitle} -c copy $syncedSubtitle
  rm $subtitle
  mv $syncedSubtitle $subtitle
fi

sourceResolution="$(ffprobe -v error -select_streams v:0 -show_entries stream=width,height -of csv=s=x:p=0 ${source})"
arrIN=(${sourceResolution//x/ })
sourceWidth="${arrIN[0]}"
sourceHeight="${arrIN[1]}"
watermarkWidth="$(echo "scale=3; ($sourceWidth / 1920) * 200" | bc)"
watermarkWidth=${watermarkWidth%.*}

echo $watermarkWidth
if [ $((watermarkWidth % 2)) -ne 0 ];then
  echo "width is odd."
  watermarkWidth=$((watermarkWidth - 1))
fi
echo $watermarkWidth

if [[ ! "${subtitle}" ]]; then
  ffmpeg -hide_banner -y -i ${source} -i ${watermark} -filter_complex "[1:v]scale=${watermarkWidth}:-1[wm];[0:v][wm]overlay=15:15" -crf ${HLS_CRF} ${target}
else
  ffmpeg -hide_banner -y -i ${source} -i ${watermark} -filter_complex "[1:v]scale=${watermarkWidth}:-1[wm];[0:v][wm]overlay=15:15,subtitles=${subtitle}:fontsdir=./fonts:force_style=ArialMT" -crf ${HLS_CRF} ${target}
fi

