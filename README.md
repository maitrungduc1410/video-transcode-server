# Intro
This project is a simple NodeJS (Express + Multer) server that receive video and transcode to HLS then upload to Azure storage
# Run locally
NodeJS is required

Run the following command to install dependencies:
```
npm install
```

Next run the following commands:
```
export STORAGE_ACCOUNT=<ENTER YOUR AZURE STORAGE ACCOUNT HERE>
export STORAGE_ACCESS_KEY=<ENTER YOUR AZURE STORAGE ACCOUNT KEY HERE>
export TZ=Asia/Singapore
export HLS_SEGMENT_TARGET_DURATION=10
export HLS_CRF=19
export HLS_WATERMARK_SOURCE=watermark.png
```

Then start the project using:
```
npm run dev
```
# Run with Docker
First build Docker image using command:
```
docker build -t registry.gitlab.com/maitrungduc1410/video-transcode-server .
```

Next, Replace Azure storage account credentials in `docker-compose.yml`

Now bootstrap the project:
```
docker-compose up -d
```
# Test
Open `index.html` and update the `servers` array with your server's address, in this case it's `http://localhost:3000`

Now you're ready to go
# App flow
```
                     Upload                       push job to queue             preprocessing                generate HLS
Client (index.html) --------> Server (server.js) -------------------> queue.js --------------> preprocess.sh -------------> create-vod-hls.sh
```