#!/bin/bash -l

export HLS_WATERMARK_SOURCE=watermark.png
export HLS_CRF=20
export HLS_SEGMENT_TARGET_DURATION=10

name=test-14101995
sourceDir="uploads/${name}"
source="${sourceDir}/video.mp4"
subtitle="${sourceDir}/sub.srt"
processed="processed/${name}.mp4"
output="output/${name}"

bash preprocess.sh $source $subtitle $processed
rm -rf $sourceDir

bash create-vod-hls.sh ${processed} ${output}
rm $processed

# append hls_playlist_type = vod to m3u8 output
for f in "$output"/*
do
  dir=$(dirname "${f}")
  basename=$(basename $f)
  extension=${basename##*.}
  filename="${basename%.*}"

  if [[ ($extension == m3u8 && $filename != playlist) ]];then
    echo $basename
    sed 5i#EXT-X-PLAYLIST-TYPE:VOD $f > "${dir}/new_${basename}"
    rm $f
    mv "${dir}/new_${basename}" $f
  fi
done

# upload to cloud storage
source venv/bin/activate
export AZURE_STORAGE_CONNECTION_STRING="DefaultEndpointsProtocol=https;AccountName=bubblestorageacc;AccountKey=sLnN4z07e2dsHIkUaACNqK92ObnIDdMmABogKz/wyPlSvIsdyYrQa8kEB87EpKc/8ftoTFGnYeEfYkLPkBAbuw==;EndpointSuffix=core.windows.net"
python upload.py $output

rm -rf $output

echo ----FINISH----