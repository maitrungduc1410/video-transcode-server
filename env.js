module.exports = {
  uploadStorage: 'storage/uploads',
  outputStorage: 'storage/outputs',
  redisHost: process.env.IS_DOCKER ? 'redis' : '127.0.0.1',
  storageAccount: process.env.STORAGE_ACCOUNT,
  storageAccessKey: process.env.STORAGE_ACCESS_KEY
}